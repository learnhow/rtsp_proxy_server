QT += core
QT -= gui
TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle

DESTDIR = ../bin

HEADERS += \
    rtsp_packet_parser.h \
    rtsp_proxy_type.h \
    udp_pipe.h \
    thread_pool.h \
    stream_thread.h \
    rtsp_proxy_server.h \
    rtsp_proxy_client.h \
    config.h \
    play_connect_handler.h \
    stream_task.h

SOURCES += main.cpp \
    rtsp_packet_parser.cpp \
    udp_pipe.cpp \
    thread_pool.cpp \
    stream_thread.cpp \
    rtsp_proxy_server.cpp \
    rtsp_proxy_client.cpp \
    config.cpp \
    play_connect_handler.cpp \
    stream_task.cpp

INCLUDEPATH += ../include/ \

LIBS += -levent \

