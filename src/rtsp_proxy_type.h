#ifndef RTSP_PROXY_TYPE_H
#define RTSP_PROXY_TYPE_H

#include <cstring>

namespace rtsp {
struct RTSPPort
{
    RTSPPort()
    {
        rtp_port = 0;
        rtcp_port = 0;
    }
    int rtp_port;
    int rtcp_port;
};

struct CameraData
{
    CameraData()
    {
        memset(host, 0, 16);
        port = 0;
        channel_num = 0;
    }

    char host[16];
    int port;
    int channel_num;
};

enum ConnectEnum
{
    CONNECTION, DISCONNECTION
};

enum RtspMethodEnum
{
    RM_NONE, RM_OPTIONS, RM_DESCRIBE, RM_SETUP, RM_PLAY, RM_GET_PARAMETER, RM_PAUSE, RM_TEARDOWN
};
}


#endif // RTSP_PROXY_TYPE_H
