#ifndef RTSP_RPROXY_SERVER_H
#define RTSP_RPROXY_SERVER_H

#ifdef _WIN32
#define evutil_socket_t intptr_t
#else
#define evutil_socket_t int
#endif

/**
 * @brief 代理服务
 * @details 用来代理所有摄像头的rtsp地址并等待播放端连接
 */
class RTSPProxyServer
{
public:
    static RTSPProxyServer* NewCreate(unsigned short listener_port);
    /**
     * @brief Connected 服务端连接回调
     * @param listen
     * @param sock 客户端网络套接字
     * @param addr
     * @param len
     * @param ctx 回调参数
     */
    static void Connected(struct evconnlistener *listen, evutil_socket_t sock, struct sockaddr *addr, int len, void *ctx);

    /**
     * @brief Init 服务初始化
     * @return
     */
    bool Init();

    /**
     * @brief Connect 开始监听
     */
    void Connect();

    bool exit_loop() const;
    void set_exit_loop(bool is_loop);

    /**
     * @brief ActionLoop 循环动作，在事件循环内部执行
     */
    virtual void ActionLoop();

    /**
     * @brief ActionInit 继承对象自己实现需要初始化的方法
     */
    virtual bool ActionInit();

    /**
     * @brief ActionFree 继承对象实现在退出事件循环时释放资源
     */
    virtual void ActionFree();
    virtual ~RTSPProxyServer();
private:
    RTSPProxyServer(unsigned short listener_port);

    bool exit_loop_;
    unsigned short listener_port_;
    struct event_base *event_base_ = nullptr;
    struct evconnlistener *conn_listener_ = nullptr;
    int backlog_;
};

#endif // RTSP_RPROXY_SERVER_H
