#ifndef STREAM_TASK_H
#define STREAM_TASK_H

#include "rtsp_proxy_type.h"
#include "udp_pipe.h"

/**
 * @brief 管线任务
 */
class StreamTask
{
public:
    StreamTask();
    /**
     * @brief PlayerHost 播放端主机地址
     * @param host
     * @return
     */
    StreamTask* PlayerHost(const char* host);
    /**
     * @brief PlayerPorts 播放端端口信息
     * @param player_rtsp_port
     * @param proxy_rtsp_port
     * @return
     */
    StreamTask* PlayerPorts(rtsp::RTSPPort player_rtsp_port, rtsp::RTSPPort proxy_rtsp_port);
    /**
     * @brief CameraHost 摄像机主机地址
     * @param host
     * @return
     */
    StreamTask* CameraHost(const char* host);
    /**
     * @brief CameraPorts 摄像机端口信息
     * @param camera_rtsp_port
     * @param proxy_rtsp_port
     * @return
     */
    StreamTask* CameraPorts(rtsp::RTSPPort camera_rtsp_port, rtsp::RTSPPort proxy_rtsp_port);
    /**
     * @brief Build 创建管线
     * @param eb
     * @return
     */
    bool Build(struct event_base* eb);
    virtual ~StreamTask();
private:
    char player_host_[16] = {0};
    char camera_host_[16] = {0};
    rtsp::RTSPPort player_rtsp_port_;
    rtsp::RTSPPort player_proxy_rtsp_port_;
    rtsp::RTSPPort camera_rtsp_port_;
    rtsp::RTSPPort camera_proxy_rtsp_port_;
    // 管线对象
    UDPPipe *rtp_pipe_ = nullptr;
    UDPPipe *rtcp_pipe_ = nullptr;
};

#endif // STREAM_TASK_H
