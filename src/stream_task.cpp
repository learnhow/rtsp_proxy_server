#include "stream_task.h"
#include <cstring>
#include "event2/event.h"
#include <iostream>
using namespace std;
using namespace rtsp;

StreamTask::StreamTask()
{
    memset(player_host_, 0, 16);
    memset(camera_host_, 0, 16);
}

StreamTask *StreamTask::PlayerHost(const char *host)
{
    memcpy(player_host_, host, strlen(host));
    return this;
}

StreamTask *StreamTask::PlayerPorts(RTSPPort player_rtsp_port, RTSPPort proxy_rtsp_port)
{
    player_rtsp_port_ = player_rtsp_port;
    player_proxy_rtsp_port_ = proxy_rtsp_port;
    return this;
}

StreamTask *StreamTask::CameraHost(const char *host)
{
    memcpy(camera_host_, host, strlen(host));
    return this;
}

StreamTask *StreamTask::CameraPorts(RTSPPort camera_rtsp_port, RTSPPort proxy_rtsp_port)
{
    camera_rtsp_port_ = camera_rtsp_port;
    camera_proxy_rtsp_port_ = proxy_rtsp_port;
    return this;
}

bool StreamTask::Build(event_base* eb)
{
    // 创建rtp管线
    rtp_pipe_ = new UDPPipe(player_proxy_rtsp_port_.rtp_port,
                            camera_proxy_rtsp_port_.rtp_port,
                            player_host_,
                            player_rtsp_port_.rtp_port);
    rtp_pipe_->set_event_base(eb);
    rtp_pipe_->Init();
    // 创建rtcp管线
    rtcp_pipe_ = new UDPPipe(camera_proxy_rtsp_port_.rtcp_port,
                             player_proxy_rtsp_port_.rtcp_port,
                             camera_host_,
                             camera_rtsp_port_.rtcp_port);
    rtcp_pipe_->set_event_base(eb);
    rtcp_pipe_->Init();
    return true;
}

StreamTask::~StreamTask()
{
    if(rtp_pipe_)
    {
        delete rtp_pipe_;
    }
    if(rtcp_pipe_)
    {
        delete rtcp_pipe_;
    }
}
