#ifndef RTSP_CONNECTHANDLER_H
#define RTSP_CONNECTHANDLER_H

#include "rtsp_proxy_client.h"
#include "rtsp_packet_parser.h"
#include "udp_pipe.h"
#include "config.h"

/**
 * @brief 处理来自一个播放端的请求和响应，等待播放器断开连接时自己释放
 */
class PlayConnectHandler
{
public:
    /**
     * @brief NewCreate 创建新的连接处理器
     * @param base event_base
     * @param socket 播放端的网咯套接字
     * @param player_host 播放端的主机地址
     * @param player_port 连接端口
     * @return
     */
    static PlayConnectHandler* NewCreate(struct event_base* base, int socket, const char* player_host, int player_port);
    /**
     * @brief OnRead 播放端数据接收回调
     */
    static void OnRead(struct bufferevent *bev, void *ctx);
    /**
     * @brief OnWrite 向播放端发送应答完成后的回调
     */
    static void OnWrite(struct bufferevent *bev, void *ctx);
    /**
     * @brief OnEvent 播放端其他事件的触发回调
     */
    static void OnEvent(struct bufferevent *bev, short what, void *ctx);

    /**
     * @brief OnPeerConnected 对向连接事件回调
     * @param type 连接事件类型
     * @param ret 结果
     * @param arg 回调参数
     */
    static void OnCameraConnected(rtsp::ConnectEnum type, void* arg);

    /**
     * @brief OnPeerEvent 对向RTSP握手事件回调
     * @param type 方法类型
     * @param msg 消息
     * @param arg 回调参数
     */
    static void OnCameraMethodEvent(rtsp::RtspMethodEnum type, char *msg, void* arg);
    virtual ~PlayConnectHandler();
private:
    PlayConnectHandler(struct event_base* base, int socket, const char* player_host, int player_port);
    /**
     * @brief GetRequestChannelNumber 从播放器发送的报文中解析请求的通道
     * @param in
     * @return
     */
    int GetRequestChannelNumber(const char *in);
    /**
     * @brief ReplacePacket 替换报文中的主机地址，主机端口和通道号
     * @param in 输入报文
     * @param out 输出报文
     * @param host
     * @param port
     * @param channel_num
     */
    void ReplacePacket(const char* in, char* out, const char* host, int port, int channel_num);
    char player_host_[16] = {0};
    int player_port_;
    RTSPProxyClient *proxy_client_ = nullptr;
    RTSPPacketParser parser_;
    struct bufferevent* bev_ = nullptr;
    char *data_ = nullptr; // 收发报文的缓存
    int data_size_ = 1024; // 收发报文的最大长度
};

#endif // RTSP_CONNECTHANDLER_H
