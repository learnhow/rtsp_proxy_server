#ifndef STREAM_THREAD_H
#define STREAM_THREAD_H

#include <pthread.h>
#include "stream_task.h"
#include <list>

/**
 * @brief 内部线程对象，由线程池管理
 */
class StreamThread
{
public:
    /**
     * @brief StreamThread
     * @param id 指定线程ID
     */
    StreamThread(int id);
    /**
     * @brief AddTask 添加任务
     * @param task
     */
    void AddTask(StreamTask* task);
    /**
     * @brief RemoveTask 移除任务
     * @param task
     */
    void RemoveTask(StreamTask* task);
    /**
     * @brief IsContain 判断当前现成是否包含任务
     * @param task
     * @return
     */
    bool IsContain(StreamTask* task);
    /**
     * @brief TaskSize 线程的任务数量
     * @return
     */
    int TaskSize();
    /**
     * @brief Start
     */
    void Start();
    /**
     * @brief Stop
     */
    void Stop();
    static void* Run(void *arg);
private:
    int id_;
    int stream_sum_;
    struct event_base* base_ = nullptr;
    pthread_t th_;
    bool is_exit_;
    std::list<StreamTask*> task_list_;
};

#endif // STREAM_THREAD_H
