#include "config.h"
#include <cstring>
#include <cstdlib>
#include <QFile>
#include <QString>
#include <iostream>
#include <sstream>
#include <QDebug>

using namespace std;
using namespace rtsp;

Config::Config()
{

}

void Config::ReadChannels()
{
    stringstream ss;
    while(!xml_reader_.atEnd())
    {
        ss.str("");
        xml_reader_.readNext();
        if(xml_reader_.isEndElement())
        {
            if(xml_reader_.name() == "channels")
            {
                return;
            }
        }
        if(xml_reader_.isStartElement())
        {
            if(xml_reader_.name() == "channel")
            {
                char* host = xml_reader_.attributes().value("host").toLocal8Bit().data();
                int port = xml_reader_.attributes().value("port").toInt();
                int camera_channel = xml_reader_.attributes().value("channel").toInt();
                QXmlStreamReader::TokenType token = xml_reader_.readNext();
                if(token == QXmlStreamReader::Characters)
                {
                    int proxy_chennel = xml_reader_.text().toInt();
                    CameraData data;
                    memcpy(data.host, host, strlen(host));
                    data.port = port;
                    data.channel_num = camera_channel;
                    channel_table_[proxy_chennel] = data;
                }
            }
        }
    }
}

bool Config::Init(const char* xml_path)
{
    QString file_name(xml_path);
    QFile fp(file_name);
    if(!fp.open(QFile::ReadOnly | QFile::Text))
    {
        qCritical("配置文件(%s)未找到", xml_path);
        return false;
    }
    xml_reader_.setDevice(&fp);
    stringstream ss;
    while(!xml_reader_.atEnd())
    {
        ss.str("");
        xml_reader_.readNext();
        if(xml_reader_.isStartElement())
        {
            if(xml_reader_.name() == "range")
            {
                int min = xml_reader_.attributes().value("min").toInt();
                int max = xml_reader_.attributes().value("max").toInt();
                port_range_.first = min;
                port_range_.second = max;
            }
            if(xml_reader_.name() == "listen")
            {
                server_port_ = xml_reader_.attributes().value("port").toInt();
            }
            if(xml_reader_.name() == "pool")
            {
                thread_pool_size_ = xml_reader_.attributes().value("size").toInt();
            }
            if(xml_reader_.name() == "channels")
            {
                ReadChannels();
            }
        }
    }
    fp.close();
    qInfo("配置文件(%s)加载完成", xml_path);
    return true;
}

pair<RTSPPort, RTSPPort> Config::NextDoublePipePort()
{
    pair<RTSPPort, RTSPPort> ret;
    RTSPPort play_ports;
    RTSPPort camera_ports;
    if(next_pipe_port_ > port_range_.second - 4)
    {
        next_pipe_port_ = port_range_.first;
    }
    // 从4n号端口开始，获取连续的4个端口号作为代理端口
    play_ports.rtp_port = next_pipe_port_++;
    play_ports.rtcp_port = next_pipe_port_++;
    camera_ports.rtp_port = next_pipe_port_++;
    camera_ports.rtcp_port = next_pipe_port_++;
    ret.first = play_ports;
    ret.second = camera_ports;
    return ret;
}

int Config::GetServerPort()
{
    return server_port_;
}

string Config::GetServerHost()
{
    return server_host_;
}

pair<int, int>* Config::GetPortRange()
{
    return &port_range_;
}

map<int, CameraData>* Config::GetChannelTable()
{
    return &channel_table_;
}

int Config::ThreadPoolSize()
{
    return thread_pool_size_;
}

CameraData *Config::GetCameraInfoByChannel(int channel)
{
    map<int, CameraData>::iterator it = channel_table_.find(channel);
    if(it != channel_table_.end())
    {
        return &(it->second);
    }
    return NULL;
}

