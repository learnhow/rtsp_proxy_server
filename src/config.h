#ifndef CONFIG_H
#define CONFIG_H

#include <map>
#include "rtsp_proxy_type.h"
#include <string>
#include <QXmlStreamReader>

/**
 * @brief 服务配置实例化工具
 */
class Config
{
public:
    static Config* Get()
    {
        static Config instance;
        return &instance;
    }

    /**
     * @brief Init 读取配置文件并初始化相关参数
     * @param xml_path xml文件路径
     * @return
     */
    bool Init(const char* xml_path);
    /**
     * @brief NextDoublePipePort 获取两对rtp和rtcp通道端口
     * @return
     */
    std::pair<rtsp::RTSPPort, rtsp::RTSPPort> NextDoublePipePort();

    /**
     * @brief GetServerPort 代理服务监听端口
     * @return
     */
    int GetServerPort();

    /**
     * @brief GetServerHost 代理服务本机地址
     * @return
     */
    std::string GetServerHost();

    /**
     * @brief GetPortRange 代理端口的范围，指定的端口范围必须从偶数端口开始奇数端口结束且范围应该是4的倍数
     * @return
     */
    std::pair<int, int>* GetPortRange();
    /**
     * @brief GetChannelTable 代理服务的通道到摄像机的映射表
     * @return
     */
    std::map<int, rtsp::CameraData>* GetChannelTable();
    /**
     * @brief ThreadPoolSize 内部线程池的大小，线程池用来均衡负载所有rtp和rtcp报文，考虑到线程切换的开销不应该配置过多的线程
     * @return
     */
    int ThreadPoolSize();
    /**
     * @brief GetCameraInfoByChannel 根据通道号得到摄像机地址信息
     * @param channel
     * @return
     */
    rtsp::CameraData* GetCameraInfoByChannel(int channel);
private:
    Config();
    void ReadChannels();
    int server_port_;
    std::string server_host_;
    std::pair<int, int> port_range_;
    std::map<int, rtsp::CameraData> channel_table_;
    int next_pipe_port_;
    int thread_pool_size_;
    QXmlStreamReader xml_reader_;
};

#endif // CONFIG_H
