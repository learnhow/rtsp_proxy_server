#ifndef RTSP_PACKET_PARSER_H
#define RTSP_PACKET_PARSER_H
#include "rtsp_proxy_type.h"

class RTSPPacketParser
{
public:
    RTSPPacketParser();
    rtsp::RTSPPort GetClientPort(const char* in);
    void ReplaceClientPort(const char* in, char* out, rtsp::RTSPPort ports);
    rtsp::RTSPPort GetServerPort(const char* in);
    void ReplaceServerPort(const char* in, char* out, rtsp::RTSPPort ports);
private:
};

#endif // RTSP_PACKET_PARSER_H
