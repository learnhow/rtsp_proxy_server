#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include <list>
#include "stream_thread.h"
#include "stream_task.h"

/**
 * @brief 线程池
 */
class ThreadPool
{
public:
    static ThreadPool* Get();
    /**
     * @brief Init 初始化线程池
     * @param pool_size
     */
    void Init(int pool_size);
    /**
     * @brief 向线程池中添加任务,内部实现负载均衡
     * @param task
     */
    void AddTask(StreamTask* task);
    /**
     * @brief 移除任务
     * @param task
     */
    void RemoveTask(StreamTask* task);
    /**
     * @brief 清理和销毁所有线程
     */
    void Destroy();
private:
    ThreadPool();
    std::list<StreamThread*> thread_list_;
};

#endif // THREAD_POOL_H
