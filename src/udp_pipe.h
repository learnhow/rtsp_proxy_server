#ifndef UDP_PIPE_H
#define UDP_PIPE_H


/**
 * @brief UDP管线对象，监听指定listen端口并将所有接收到的UDP报文从send端口发送给远程主机上的dest端口
 */
class UDPPipe
{
public:
    /**
     * @brief UDPPipe
     * @param listen_port 监听端口
     * @param send_port 转发端口
     * @param dest_host 目标主机地址
     * @param dest_port 目标主机端口
     */
    UDPPipe(int listen_port, int send_port, const char* dest_host, int dest_port);
    virtual ~UDPPipe();
    void Init();
    static void OnRead(int fd, short what, void *cbarg);
    void set_event_base(struct event_base* eb);

private:
    struct event_base* eb_;
    int listen_port_;
    int send_port_;
    int dest_port_;
    char dest_host_[16];
    int listen_sock_;
    int send_sock_;
    struct event* recv_ev_ = nullptr;
    char *buf_ = nullptr;
};

#endif // UDP_PIPE_H
