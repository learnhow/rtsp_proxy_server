#include <iostream>
#include <cstring>
#include <stdlib.h>
#include "rtsp_proxy_server.h"
#include "config.h"

int main(int argc, char *argv[])
{
    Config *conf = Config::Get();
    if(conf->Init("proxy.xml"))
    {
        RTSPProxyServer *proxy = RTSPProxyServer::NewCreate(conf->GetServerPort());
        if(proxy->Init())
        {
            proxy->Connect();
        }
    }

    return 0;
}
